# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

timeout: 7200s # 2hr
tags:
  - main
substitutions:
  _PROJECT_ID: ${PROJECT_ID}
  _INFRA: "true"
  _APPS: "true"
  _ASMXLB: "true"
  _MCIXLB: "true"
  _ASMILB: "true"
  _MCIILB: "true"
  _ASMLLB: "true"
options:
  substitution_option: 'ALLOW_LOOSE'
steps:
# This step creates the infrastructure required for this tutorial.
# This step creates the following resources:
#   1. 4 GKE clusters, 3 application clusters and 1 admin/config cluster for configuring multicluster ingress.
#   2. Registers all clusters to Fleet (in the same project)
#   3. Enables multicluster services (MCS).
#   4. Enables multicluster gateways (MCG).
#   5. Enables ASM.
- name: "gcr.io/cloud-builders/gcloud"
  id: "00-infra"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_INFRA}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/infra.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys sample applications on the application clusters.
# Following applications are deployed to the 3 application clusters:
#   1. Bank of Anthos is deployed in the bank-of-anthos namespace.
#   2. Online Boutique is deployed in the online-boutique namespace.
#   3. Whereami sample GKE app is deployed in the whereami namespace. Both whereami-frontend and whereami-backend are deployed in the whereami namespace. These are used for locality load balancing.
#   4. Whereami is used to verify multicluster mesh connectivity. Applications running in the 3 app clusters can discover and route to each other.
- name: "gcr.io/cloud-builders/gcloud"
  id: "01-apps"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_APPS}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/apps.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys external ASM ingress gateways in the 3 app clusters.
# These ASM ingress gateways are used for traffic originating externally (outside VPC) to the mesh.
- name: "gcr.io/cloud-builders/gcloud"
  id: "02-asmxlb"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_ASMXLB}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/asmxlb.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys external (traffic originating from outside of the VPC or corp network) multicluster ingress for all 3 applications.
# Following resources are deployed:
#   1. Cloud Endpoints DNS names for the 3 apps. Free DNS names courtesy of Cloud Endpoints.
#   2. Google managed certs for the 3 apps (using Cloud Endpoint DNS names).
#   3. Cloud Armore security policy.
#   4. Self signed OpenSSL cert for traffic encryption between GCLB and ASM ingress gateways.
#   5. MulitclusterIngress (MCI) and MulticlusterServices (MCS) resources.
#   6. ASM Gateway and VirtualService resources for the 3 apps.
- name: "gcr.io/cloud-builders/gcloud"
  id: "03-mcixlb"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_MCIXLB}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/mcixlb.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys internal ASM ingress gateways in the 3 app clusters.
# These ASM ingress gateways are used for traffic originating internally (inside the VPC or corp network but outside of the mesh) to inside the mesh.
- name: "gcr.io/cloud-builders/gcloud"
  id: "04-asmilb"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_ASMILB}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/asmilb.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys internal (traffic originating from inside of the VPC or corp network) multicluster ingress for all 3 applications.
# Following resources are deployed:
#   1. Kubernetes Gateway resource (using the rilb-mc Gateway class)
#   2. HTTPRoute resources
#   3. ASM VirtualService resources
- name: "gcr.io/cloud-builders/gcloud"
  id: "05-mciilb"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_MCIILB}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/mciilb.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}
# This step deploys internal (traffic originating from inside of the VPC or corp network) multicluster ingress for all 3 applications.
# Following resources are deployed:
#   1. Kubernetes Gateway resource (using the rilb-mc Gateway class)
#   2. HTTPRoute resources
#   3. ASM VirtualService resources
- name: "gcr.io/cloud-builders/gcloud"
  id: "06-asmllb"
  entrypoint: bash
  args:
    - -c
    - |
      [[ "${_ASMLLB}" == "false" ]] && exit 0
      exec gcloud builds submit --config infra/asmllb.yaml --substitutions=_PROJECT_ID=${_PROJECT_ID}