/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

provider "google" {
  region = var.region
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

module "gke" {
  source       = "terraform-google-modules/kubernetes-engine/google//modules/beta-autopilot-private-cluster"
  project_id = var.project_id
  regional   = true
  region     = var.region
  zones      = [var.zone]
  gateway_api_channel = "CHANNEL_STANDARD"

  name = var.cluster_name

  network           = data.terraform_remote_state.vpc.outputs.network_name
  subnetwork        = data.terraform_remote_state.vpc.outputs.subnets_names[1]
  ip_range_pods     = data.terraform_remote_state.vpc.outputs.subnets_secondary_ranges[1][0].range_name
  ip_range_services = data.terraform_remote_state.vpc.outputs.subnets_secondary_ranges[1][1].range_name

  release_channel                 = "REGULAR"
  enable_vertical_pod_autoscaling = true

  service_account = "create"
  enable_private_endpoint    = false
  enable_private_nodes       = true
  master_ipv4_cidr_block     = var.private_ipv4_cidr
  
  master_authorized_networks = var.google_static_ips
}
