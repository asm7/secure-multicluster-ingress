/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "cluster_name" {
  description = "Cluster name"
  default     = "gke-central-1"
}

variable "region" {
  description = "The region to host the cluster in"
  default     = "us-central1"
}

variable "zone" {
  type        = string
  description = "The zone to host the cluster in"
  default     = "us-central1-a"
}

variable "mesh_feature" {
  type        = string
  description = "Mesh feature name"
  default     = "servicemesh"
}

variable "private_ipv4_cidr" {
    type    = string
    description = "IPV4 CIDR range for Private GKE clusters API"
    default = "10.200.1.0/28"
}

variable "google_static_ips" {
    type    = list(object({
    cidr_block = string
    display_name = string
  }))
  description = "List of Google publicly advertised static IP addresses"
  default = [{
    cidr_block= "10.0.0.0/8"
    display_name = "vpc-prod"
},{
    cidr_block= "8.8.4.0/24"
    display_name = "google_static_ips_01"
}, {
    cidr_block= "8.8.8.0/24"
    display_name = "google_static_ips_02"
}, {
    cidr_block= "8.34.208.0/20"
    display_name = "google_static_ips_03"
}, {
    cidr_block= "8.35.192.0/20"
    display_name = "google_static_ips_04"
}, {
    cidr_block= "23.236.48.0/20"
    display_name = "google_static_ips_05"
}, {
    cidr_block= "23.251.128.0/19"
    display_name = "google_static_ips_06"
}, {
    cidr_block= "34.0.0.0/15"
    display_name = "google_static_ips_07"
}, {
    cidr_block= "34.2.0.0/16"
    display_name = "google_static_ips_08"
}, {
    cidr_block= "34.3.0.0/23"
    display_name = "google_static_ips_09"
}, {
    cidr_block= "34.3.3.0/24"
    display_name = "google_static_ips_10"
}, {
    cidr_block= "34.3.4.0/24"
    display_name = "google_static_ips_11"
}, {
    cidr_block= "34.3.8.0/21"
    display_name = "google_static_ips_12"
}, {
    cidr_block= "34.3.16.0/20"
    display_name = "google_static_ips_13"
}, {
    cidr_block= "34.3.32.0/19"
    display_name = "google_static_ips_14"
}, {
    cidr_block= "34.3.64.0/18"
    display_name = "google_static_ips_15"
}, {
    cidr_block= "34.3.128.0/17"
    display_name = "google_static_ips_16"
}, {
    cidr_block= "34.4.0.0/14"
    display_name = "google_static_ips_17"
}, {
    cidr_block= "34.8.0.0/13"
    display_name = "google_static_ips_18"
}, {
    cidr_block= "34.16.0.0/12"
    display_name = "google_static_ips_19"
}, {
    cidr_block= "34.32.0.0/11"
    display_name = "google_static_ips_20"
}, {
    cidr_block= "34.64.0.0/10"
    display_name = "google_static_ips_21"
}, {
    cidr_block= "34.128.0.0/10"
    display_name = "google_static_ips_22"
}, {
    cidr_block= "35.184.0.0/13"
    display_name = "google_static_ips_23"
}, {
    cidr_block= "35.192.0.0/14"
    display_name = "google_static_ips_24"
}, {
    cidr_block= "35.196.0.0/15"
    display_name = "google_static_ips_25"
}, {
    cidr_block= "35.198.0.0/16"
    display_name = "google_static_ips_26"
}, {
    cidr_block= "35.199.0.0/17"
    display_name = "google_static_ips_27"
}, {
    cidr_block= "35.199.128.0/18"
    display_name = "google_static_ips_28"
}, {
    cidr_block= "35.200.0.0/13"
    display_name = "google_static_ips_29"
}, {
    cidr_block= "35.208.0.0/12"
    display_name = "google_static_ips_30"
}, {
    cidr_block= "35.224.0.0/12"
    display_name = "google_static_ips_31"
}, {
    cidr_block= "35.240.0.0/13"
    display_name = "google_static_ips_32"
}, {
    cidr_block= "64.15.112.0/20"
    display_name = "google_static_ips_33"
}, {
    cidr_block= "64.233.160.0/19"
    display_name = "google_static_ips_34"
}, {
    cidr_block= "66.22.228.0/23"
    display_name = "google_static_ips_35"
}, {
    cidr_block= "66.102.0.0/20"
  display_name = "google_static_ips_36"
}, {
    cidr_block= "66.249.64.0/19"
  display_name = "google_static_ips_37"
}, {
    cidr_block= "70.32.128.0/19"
  display_name = "google_static_ips_38"
}, {
    cidr_block= "72.14.192.0/18"
  display_name = "google_static_ips_39"
}, {
    cidr_block= "74.114.24.0/21"
  display_name = "google_static_ips_40"
}, {
    cidr_block= "74.125.0.0/16"
  display_name = "google_static_ips_41"
}, {
    cidr_block= "104.154.0.0/15"
  display_name = "google_static_ips_42"
}, {
    cidr_block= "104.196.0.0/14"
  display_name = "google_static_ips_43"
}, {
    cidr_block= "104.237.160.0/19"
  display_name = "google_static_ips_44"
}, {
    cidr_block= "107.167.160.0/19"
  display_name = "google_static_ips_45"
}, {
    cidr_block= "107.178.192.0/18"
  display_name = "google_static_ips_46"
}, {
    cidr_block= "108.59.80.0/20"
  display_name = "google_static_ips_47"
}, {
    cidr_block= "108.170.192.0/18"
  display_name = "google_static_ips_48"
}, {
    cidr_block= "108.177.0.0/17"
  display_name = "google_static_ips_49"
}, {
    cidr_block= "130.211.0.0/16"
  display_name = "google_static_ips_50"
}, {
    cidr_block= "136.112.0.0/12"
  display_name = "google_static_ips_51"
}, {
    cidr_block= "142.250.0.0/15"
  display_name = "google_static_ips_52"
}, {
    cidr_block= "146.148.0.0/17"
  display_name = "google_static_ips_53"
}, {
    cidr_block= "162.216.148.0/22"
  display_name = "google_static_ips_54"
}, {
    cidr_block= "162.222.176.0/21"
  display_name = "google_static_ips_55"
}, {
    cidr_block= "172.110.32.0/21"
  display_name = "google_static_ips_56"
}, {
    cidr_block= "172.217.0.0/16"
  display_name = "google_static_ips_57"
}, {
    cidr_block= "172.253.0.0/16"
  display_name = "google_static_ips_58"
}, {
    cidr_block= "173.194.0.0/16"
  display_name = "google_static_ips_59"
}, {
    cidr_block= "173.255.112.0/20"
  display_name = "google_static_ips_60"
}, {
    cidr_block= "192.158.28.0/22"
  display_name = "google_static_ips_61"
}, {
    cidr_block= "192.178.0.0/15"
  display_name = "google_static_ips_62"
}, {
    cidr_block= "193.186.4.0/24"
  display_name = "google_static_ips_63"
}, {
    cidr_block= "199.36.154.0/23"
  display_name = "google_static_ips_64"
}, {
    cidr_block= "199.36.156.0/24"
  display_name = "google_static_ips_65"
}, {
    cidr_block= "199.192.112.0/22"
  display_name = "google_static_ips_66"
}, {
    cidr_block= "199.223.232.0/21"
  display_name = "google_static_ips_67"
}, {
    cidr_block= "207.223.160.0/20"
  display_name = "google_static_ips_68"
}, {
    cidr_block= "208.65.152.0/22"
  display_name = "google_static_ips_69"
}, {
    cidr_block= "208.68.108.0/22"
  display_name = "google_static_ips_70"
}, {
    cidr_block= "208.81.188.0/22"
  display_name = "google_static_ips_71"
}, {
    cidr_block= "208.117.224.0/19"
  display_name = "google_static_ips_72"
}, {
    cidr_block= "209.85.128.0/17"
  display_name = "google_static_ips_73"
}, {
    cidr_block= "216.58.192.0/19"
  display_name = "google_static_ips_74"
}, {
    cidr_block= "216.73.80.0/20"
  display_name = "google_static_ips_75"
}, {
    cidr_block= "216.239.32.0/19"
  display_name = "google_static_ips_76"
}]
}