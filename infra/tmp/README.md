## Installing a v2 whereami-frontend

1. Install `whereami-frontend-v2`.

    ```bash
    kubectl apply -k k8s-frontend-overlay-example-2/ --context=${CLUSTER_1} -n whereami
    ```

1. Install `DestinationRule` and `VirtualService`.

    ```bash
    kubectl --context=${CLUSTER_1} apply -f destinationrules-whereami-frontend-cluster-1.yaml
    kubectl --context=${CLUSTER_1} apply -f whereami-virtualservice.yaml
    ```